// 
// 
//
#include "ClockWordController.h"
#include <RtcDS3231.h>

NeoPixelBrightnessBus<NeoGrbFeature, NeoEsp8266Dma800KbpsMethod> strip(NUM_LEDS);

extern ClockConfig clockConfig;
extern RenderMode renderMode;
extern byte sentenceDisplayColour;
extern byte colourIndex;
extern RtcDateTime clockTime;

//
// Initialiser
//
void ClockWordController::init(RgbColor displayCol, RgbColor sentenceCol)
{
	updateStep = false;
	updateSparkle = false;
	updateFlash = false;
	updateFade = false;
	fadeoutActive = false;
	sparkleOutActive = false;
	renderColour = displayCol;
	sentenceColour = sentenceCol;
	flashState = false;

	currentMillis = 0;
	previousMillis = 0;
	previousStepMillis = 0;
	previousFlashMillis = 0;
	previousFadeMillis = 0;

	strip.Begin();
	strip.SetBrightness(DAY_BRIGHTNESS);
	strip.Show();

	for (byte i = 0; i < NUM_CLOCK_WORDS; i++)
	{
		ClockWrd *pWord = &words[i];
		pWord->currentIndex = 0;
		pWord->currentFadeStep = 0;
		SetWordRenderColour(pWord, displayCol);
		SetWordRenderMode(pWord, RenderMode::On);
	}

	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		currentDisplayWords[i].index = 0xFF;
		newDisplayWords[i].index = 0xFF;
	}

	for (byte i = 0; i < NUM_LEDS; i++)
	{
		currentDisplayLetters[i].index = 0xFF;
		currentDisplayLetters[i].renderMode = RenderMode::Off;
		currentDisplayLetters[i].colour = RgbColor(0, 0, 0);

		newDisplayLetters[i].index = 0xFF;
		newDisplayLetters[i].renderMode = RenderMode::Off;
		newDisplayLetters[i].colour = RgbColor(0, 0, 0);
	}

	InitActiveWords();
	
	words[0].letterSize = (letters0, sizeof(letters0));
	words[0].letters = letters0;
	
	words[1].letterSize = (letters1, sizeof(letters1));
	words[1].letters = letters1;

	words[2].letterSize = (letters2, sizeof(letters2));
	words[2].letters = letters2;

	words[3].letterSize = (letters3, sizeof(letters3));
	words[3].letters = letters3;

	words[4].letterSize = (letters4, sizeof(letters4));
	words[4].letters = letters4;

	words[5].letterSize = (letters5, sizeof(letters5));
	words[5].letters = letters5;

	words[6].letterSize = (letters6, sizeof(letters6));
	words[6].letters = letters6;

	words[7].letterSize = (letters7, sizeof(letters7));
	words[7].letters = letters7;

	words[8].letterSize = (letters8, sizeof(letters8));
	words[8].letters = letters8;

	words[9].letterSize = (letters9, sizeof(letters9));
	words[9].letters = letters9;

	words[10].letterSize = (letters10, sizeof(letters10));
	words[10].letters = letters10;

	words[11].letterSize = (letters11, sizeof(letters11));
	words[11].letters = letters11;

	words[12].letterSize = (letters12, sizeof(letters12));
	words[12].letters = letters12;

	words[13].letterSize = (letters13, sizeof(letters13));
	words[13].letters = letters13;

	words[14].letterSize = (letters14, sizeof(letters14));
	words[14].letters = letters14;

	words[15].letterSize = (letters15, sizeof(letters15));
	words[15].letters = letters15;

	words[16].letterSize = (letters16, sizeof(letters16));
	words[16].letters = letters16;

	words[17].letterSize = (letters17, sizeof(letters17));
	words[17].letters = letters17;

	words[18].letterSize = (letters18, sizeof(letters18));
	words[18].letters = letters18;

	words[19].letterSize = (letters19, sizeof(letters19));
	words[19].letters = letters19;

	words[20].letterSize = (letters20, sizeof(letters20));
	words[20].letters = letters20;

	words[21].letterSize = (letters21, sizeof(letters21));
	words[21].letters = letters21;

	words[22].letterSize = (letters22, sizeof(letters22));
	words[22].letters = letters22;
	
	// Sentences
	words[23].letterSize = (letters23, sizeof(letters23));
	words[23].letters = letters23;

	words[24].letterSize = (letters24, sizeof(letters24));
	words[24].letters = letters24;

	words[25].letterSize = (letters25, sizeof(letters25));
	words[25].letters = letters25;

	words[26].letterSize = (letters26, sizeof(letters26));
	words[26].letters = letters26;

	words[27].letterSize = (letters27, sizeof(letters27));
	words[27].letters = letters27;

	words[28].letterSize = (letters28, sizeof(letters28));
	words[28].letters = letters28;

	words[29].letterSize = (letters29, sizeof(letters29));
	words[29].letters = letters29;

	words[30].letterSize = (letters30, sizeof(letters30));
	words[30].letters = letters30;

	words[31].letterSize = (letters31, sizeof(letters31));
	words[31].letters = letters31;

	words[32].letterSize = (letters32, sizeof(letters32));
	words[32].letters = letters32;

	words[33].letterSize = (letters33, sizeof(letters33));
	words[33].letters = letters33;

	words[34].letterSize = (letters34, sizeof(letters34));
	words[34].letters = letters34;

	words[35].letterSize = (letters35, sizeof(letters35));
	words[35].letters = letters35;

	words[36].letterSize = (letters36, sizeof(letters36));
	words[36].letters = letters36;

	words[37].letterSize = (letters37, sizeof(letters37));
	words[37].letters = letters37;

	words[38].letterSize = (letters38, sizeof(letters38));
	words[38].letters = letters38;

	words[39].letterSize = (letters39, sizeof(letters39));
	words[39].letters = letters39;

	words[40].letterSize = (letters40, sizeof(letters40));
	words[40].letters = letters40;

	words[41].letterSize = (letters41, sizeof(letters41));
	words[41].letters = letters41;

	words[42].letterSize = (letters42, sizeof(letters42));
	words[42].letters = letters42;

	words[43].letterSize = (letters43, sizeof(letters43));
	words[43].letters = letters43;

	words[44].letterSize = (letters44, sizeof(letters44));
	words[44].letters = letters44;

	words[45].letterSize = (letters45, sizeof(letters45));
	words[45].letters = letters45;

	words[46].letterSize = (letters46, sizeof(letters46));
	words[46].letters = letters46;

	words[47].letterSize = (letters47, sizeof(letters47));
	words[47].letters = letters47;

	words[48].letterSize = (letters48, sizeof(letters48));
	words[48].letters = letters48;

	words[49].letterSize = (letters49, sizeof(letters49));
	words[49].letters = letters49;

	words[50].letterSize = (letters50, sizeof(letters50));
	words[50].letters = letters50;

	words[51].letterSize = (letters51, sizeof(letters51));
	words[51].letters = letters51;

	words[52].letterSize = (letters52, sizeof(letters52));
	words[52].letters = letters52;

	words[53].letterSize = (letters53, sizeof(letters53));
	words[53].letters = letters53;

	words[54].letterSize = (letters54, sizeof(letters54));
	words[54].letters = letters54;

	words[55].letterSize = (letters55, sizeof(letters55));
	words[55].letters = letters55;

	words[56].letterSize = (letters56, sizeof(letters56));
	words[56].letters = letters56;

	words[57].letterSize = (letters57, sizeof(letters57));
	words[57].letters = letters57;

	// All sentences can be displayed on all days
	for (byte i = SENTENCE_START; i < SENTENCE_START + NUM_SENTENCES; i++)
	{
		words[i].showOnDay = -1;
		words[i].showOnMonth = -1;
	}

	// Set individual sentence display days/months here (1 based for readability)
}

//
//
//
void ClockWordController::SetDisplayColour(RgbColor colour)
{
	renderColour = colour;

	for (byte i = 0; i < NUM_CLOCK_WORDS; i++)
	{
		ClockWrd *pWord = &words[i];
		SetWordRenderColour(pWord, colour);
	}

	// Special case for sparkle mode
	for (byte i = 0; i < NUM_LEDS; i++)
	{
		currentDisplayLetters[i].colour = renderColour;
	}
}

//
//
//
void ClockWordController::SetSentenceColour(RgbColor colour)
{
	sentenceColour = colour;
}

//
// Main refresh loop; should be called once per Arduino loop
//
void ClockWordController::loop()
{
	UpdateTimers();
	UpdateClockWords();
	SetDayNightMode();
	Render();
}

//
// Main render handler
//
void ClockWordController::Render()
{
	// Is it time to render?
	if (updateDisplay)
	{
		// Clear display
		for (int i = 0; i < NUM_LEDS; i++)
		{
			leds[i] = RgbColor(0, 0, 0);
		}

		strip.ClearTo(RgbColor(0, 0, 0), 0, NUM_LEDS - 1);

		// If we're fading between words, we need to let the old words fade out before setting the new
		// words to fade in.
		if (fadeoutActive)
			RenderFade();
		else if (sparkleOutActive)
			RenderSparkleOut();
		else
			RenderNormal();

		// Show the display
		RgbColor pixel;
		for (int i = 0; i < NUM_LEDS; i++)
		{
			pixel = RgbColor(
				leds[i].R,
				leds[i].G,
				leds[i].B);
			strip.SetPixelColor(i, pixel);
		}
		strip.Show();
	}
}

//
// Render loop when fade out is in progress
//
void ClockWordController::RenderFade()
{
	bool fadeoutComplete = true;

	// Render the current words. The word will change it's renderMode to Off when fadeout is complete
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		if (currentDisplayWords[i].index != 0xFF)
		{
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			RenderClockWord(pWord);

			// If the word is still fading, we're not done. All words should fade equally.
			if (pWord->renderMode == RenderMode::FadeOut)
			{
				fadeoutComplete = false;
				//Serial.println(F("RenderFade: fadeoutComplete false"));
			}
		}
	}

	// If we've finished fading the current words out, we now need to fade the new words in. We do
	// this by copying newWords to currentWords, setting the renderMode AND the renderColour (which
	// will be black). FadeOut and FadeIn could just be done by brightness however this method
	// hopefully lends itself to blending between colours.
	if (fadeoutComplete)
	{
		//Serial.println(F("RenderFade: fadeoutComplete true"));
		for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
		{
			currentDisplayWords[i].index = newDisplayWords[i].index;
			currentDisplayWords[i].renderMode = RenderMode::FadeIn;
			currentDisplayWords[i].colour = newDisplayWords[i].colour;
			if (currentDisplayWords[i].index != 0xFF)
			{
				ClockWrd *pWord = &words[currentDisplayWords[i].index];
				SetWordRenderMode(pWord, RenderMode::FadeIn);
				SetWordRenderColour(pWord, currentDisplayWords[i].colour);
			}
		}

		fadeoutActive = false;
	}
}

//
// Render loop when fade out is in progress
//
void ClockWordController::RenderSparkleOut()
{
	bool sparkleOutComplete = true;

	// Need to loop through the letter indexes
	if (currentDisplayLetters[currentLetterIndex].index != 0xFF)
	{
		if (updateSparkle)
		{
			// Each time we loop we're clearing the display, so we actually need to turn ON those lights
			// that are remaining.
			byte i = currentLetterIndex;
			while (currentDisplayLetters[i].index != 0xFF)
			{
				int index = currentDisplayLetters[i].index;
				leds[index] = currentDisplayLetters[i].colour;
				i++;
			}

			currentLetterIndex++;
		}

		return;
	}

	// We've switched all the letters off; update the words themselves for tidiness
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		if (currentDisplayWords[i].index != 0xFF)
		{
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			pWord->renderMode == RenderMode::Off;
		}
	}

	// If we've finished sparkling the current letters out, we now need to sparkle the new words in. We do
	// this by copying newWords to currentWords, setting the renderMode AND the renderColour (which
	// will be black).
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		currentDisplayWords[i].index = newDisplayWords[i].index;
		currentDisplayWords[i].renderMode = RenderMode::SparkleIn;
		if (currentDisplayWords[i].index != 0xFF)
		{
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			SetWordRenderMode(pWord, RenderMode::SparkleIn);
			SetWordRenderColour(pWord, currentDisplayWords[i].colour);
		}
	}

	// Now we need to copy the newLetters to currentLetters
	for (byte i = 0; i < NUM_LEDS; i++)
	{
		currentDisplayLetters[i].index = newDisplayLetters[i].index;
		currentDisplayLetters[i].colour = newDisplayLetters[i].colour;
	}

	// Reset indexes to sparkle in
	currentLetterCount = newLetterCount;
	currentLetterIndex = 0;

	sparkleOutActive = false;
}

//
// Render loop for cases where the renderMode is not FadeOut
//
void ClockWordController::RenderNormal()
{
	// Normally each word is responsible for rendering itself independently of any other word. But
	// for sparkle modes, we want to turn individual letters in each word on randomly, so the word
	// doesn't do anything.
	// Override is used when the user is selecting various word transition options from the web page
	// but hasn't saved yet.
	if (clockConfig.OverrideDisplayMode == RenderMode::SparkleIn || clockConfig.DisplayMode == RenderMode::SparkleIn)
	{
		if (updateSparkle)
		{
			// The display is cleared each render loop so we need to light all the letters up to the current index
			for (byte i = 0; i < currentLetterIndex; i++)
			{
				int index = currentDisplayLetters[i].index;
				leds[index] = currentDisplayLetters[i].colour;
			}

			if (currentDisplayLetters[currentLetterIndex].index != 0xFF)
				currentLetterIndex++;
			else
				clockConfig.OverrideDisplayMode == RenderMode::Off;

			return;
		}
	}
	else
	{
		// Normal rendering mode
		for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
		{
			if (currentDisplayWords[i].index != 0xFF)
			{
				ClockWrd *pWord = &words[currentDisplayWords[i].index];
				RenderClockWord(pWord);
			}
		}
	}
}

//
//
//
void ClockWordController::SetDisplayWords()
{
	// Check to see if any word is set to fade or sparkle mode
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		// New display words is the most current so set that up here
		newDisplayWords[i].index = activeWords[i].index;
		newDisplayWords[i].renderMode = activeWords[i].renderMode;
		newDisplayWords[i].colour = activeWords[i].colour;

		if (activeWords[i].renderMode == RenderMode::FadeIn)
			fadeoutActive = true;
		else if (activeWords[i].renderMode == RenderMode::SparkleIn)
		{
			sparkleOutActive = true;
			currentLetterIndex = 0;
		}
	}

	// Clear old letters; RAM is cheap here so I'm assuming we can use everything
	for (byte i = 0; i < NUM_LEDS; i++)
	{
		// Release previous allocation
		newDisplayLetters[i].index = 0xFF;
		newDisplayLetters[i].renderMode = RenderMode::Off;
		newDisplayLetters[i].colour = RgbColor(0, 0, 0);
	}

	// Generate the new display letters in case we want to use them
	byte totalLetters = 0;
	byte activeWordIndex = 0;
	byte activeLetterIndex = 0;
	while (activeWords[activeWordIndex].index != 0xFF)
	{
		byte wordIndex = activeWords[activeWordIndex].index;
		byte letterSize = words[wordIndex].letterSize;
		totalLetters += letterSize - 1;
		for (byte j = 0; j < words[wordIndex].letterSize - 1; j++)
		{
			ClockWrd *pWord = &words[wordIndex];
			newDisplayLetters[activeLetterIndex].index = pgm_read_byte(pWord->letters + j);
			newDisplayLetters[activeLetterIndex].colour = activeWords[activeWordIndex].colour;

			activeLetterIndex++;
		}

		activeWordIndex++;
	}

	newLetterCount = totalLetters;

	// Randomise the array
	for (byte i = 0; i < totalLetters; i++)
	{
		byte j = random(i + 1);
		DisplayWord t = newDisplayLetters[j];
		newDisplayLetters[j] = newDisplayLetters[i];
		newDisplayLetters[i] = t;
	}

	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		// If we're fading, set the existing words to fade out (they should be set to RenderMode::On
		if (fadeoutActive)
		{
			//Serial.print(F("SetDisplayWords: fading word index: "));
			//Serial.println(currentDisplayWords[i].index);
			currentDisplayWords[i].renderMode = RenderMode::FadeOut;
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			SetWordRenderMode(pWord, RenderMode::FadeOut);
		}
		else if (sparkleOutActive)
		{
			//Serial.print(F("SetDisplayWords: sparkling out word index: "));
			//Serial.println(currentDisplayWords[i].index);
			currentDisplayWords[i].renderMode = RenderMode::SparkleOut;
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			SetWordRenderMode(pWord, RenderMode::SparkleOut);
		}
		else
		{
			currentDisplayWords[i].index = activeWords[i].index;
			currentDisplayWords[i].renderMode = activeWords[i].renderMode;
			ClockWrd *pWord = &words[currentDisplayWords[i].index];
			SetWordRenderMode(pWord, activeWords[i].renderMode);
			SetWordRenderColour(pWord, activeWords[i].colour);
		}
	}
}

//
// Updates the fade, flash, step of each individual clock word whether they're part of the current
// display set or not
//
void ClockWordController::UpdateClockWords()
{
	for (byte i = 0; i < NUM_CLOCK_WORDS; i++)
	{
		ClockWrd *pWord = &words[i];
		UpdateWord(pWord, updateStep, updateFlash, updateFade, updateSparkle);
	}
}

//
// Updates all timers used for fade, step, flash etc
void ClockWordController::UpdateTimers()
{
	updateStep = false;
	updateSparkle = false;
	updateFlash = false;
	updateFade = false;
	updateDisplay = false;

	currentMillis = millis();

	// Update all timers
	if (currentMillis - previousMillis >= pgm_read_word(&REFRESH_RATE))
	{
		previousMillis = currentMillis;
		updateDisplay = true;
	}

	if (currentMillis - previousStepMillis >= pgm_read_word(&STEP_RATE))
	{
		previousStepMillis = currentMillis;
		updateStep = true;
	}

	if (currentMillis - previousSparkleMillis >= pgm_read_word(&SPARKLE_RATE))
	{
		previousSparkleMillis = currentMillis;
		updateSparkle = true;
	}

	if (currentMillis - previousFadeMillis >= pgm_read_word(&FADE_RATE))
	{
		previousFadeMillis = currentMillis;
		updateFade = true;
	}

	if (currentMillis - previousFlashMillis >= pgm_read_word(&FLASH_RATE))
	{
		previousFlashMillis = currentMillis;
		updateFlash = true;
		flashState = !flashState;

		// Toggle the LED on the Arduino
		digitalWrite(LED_BUILTIN, flashState);
	}
}

//
// Updates an individual ClockWord structure
void ClockWordController::UpdateWord(ClockWrd *pWord, bool updateStep, bool updateFlash, bool updateFade, bool updateSparkle)
{
	switch (pWord->renderMode)
	{
	case RenderMode::Off:
		break;

	case RenderMode::On:
		break;

	case RenderMode::Step:
		if (updateStep)
		{
			if (pWord->currentIndex < pWord->letterSize)
				pWord->currentIndex++;
			else
				pWord->renderMode = RenderMode::On;
		}
		break;

	case RenderMode::FadeOut:
		if (updateFade)
		{
			if (pWord->currentFadeStep-- > 0)
			{
				pWord->currentColour.R = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEOUT_STEPS)) * pWord->currentColour.R;
				pWord->currentColour.G = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEOUT_STEPS)) * pWord->currentColour.G;
				pWord->currentColour.B = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEOUT_STEPS)) * pWord->currentColour.B;
			}

			if (pWord->currentColour.R == 0 &&
				pWord->currentColour.G == 0 &&
				pWord->currentColour.B == 0)
			{
				pWord->renderMode = RenderMode::Off;
			}
		}
		break;

	case RenderMode::FadeIn:
		if (updateFade)
		{
			if (pWord->currentFadeStep++ < FADEIN_STEPS)
			{
				pWord->currentColour.R = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEIN_STEPS)) * pWord->targetColour.R;
				pWord->currentColour.G = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEIN_STEPS)) * pWord->targetColour.G;
				pWord->currentColour.B = (pWord->currentFadeStep / (float)pgm_read_byte(&FADEIN_STEPS)) * pWord->targetColour.B;
			}

			if (pWord->currentColour.R == pWord->targetColour.R &&
				pWord->currentColour.G == pWord->targetColour.G &&
				pWord->currentColour.B == pWord->targetColour.B)
			{
				pWord->renderMode = RenderMode::On;
			}
		}
		break;

	case RenderMode::Flash:
		if (updateFlash)
		{
			if (flashState)
				pWord->currentColour = pWord->targetColour;
			else
				pWord->currentColour = RgbColor(0, 0, 0);
		}
		break;

	case RenderMode::SparkleIn:
		// Do nothing; the controller takes care of turning the individual letters on or off
		break;

	case RenderMode::SparkleOut:
		// Do nothing; the controller takes care of turning the individual letters on or off
		break;
	}
}

void ClockWordController::RenderClockWord(ClockWrd *pWord)
{
	switch (pWord->renderMode)
	{
	case RenderMode::Off:
		for (byte i = 0; i < pWord->letterSize - 1; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = RgbColor(0, 0, 0);
		}
		break;

	case RenderMode::On:
		for (byte i = 0; i < pWord->letterSize - 1; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = pWord->currentColour;
		}
		break;

	case RenderMode::Step:
		for (byte i = 0; i < pWord->currentIndex; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = pWord->currentColour;
		}
		break;

	case RenderMode::FadeOut:
		for (byte i = 0; i < pWord->currentIndex; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = pWord->currentColour;
		}
		break;

	case RenderMode::FadeIn:
		for (byte i = 0; i < pWord->currentIndex; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = pWord->currentColour;
		}
		break;

	case RenderMode::Flash:
		// Flash
		for (byte i = 0; i < pWord->currentIndex; i++)
		{
			leds[pgm_read_byte(pWord->letters + i)] = pWord->currentColour;
		}
		break;

	case RenderMode::SparkleIn:
		// Do nothing; the controller takes care of turning the individual letters on or off
		break;

	case RenderMode::SparkleOut:
		// Do nothing; the controller takes care of turning the individual letters on or off
		break;
	}
}

void ClockWordController::SetWordRenderColour(ClockWrd *pWord, RgbColor colour)
{
	pWord->currentColour = colour;
	pWord->targetColour = colour;
}

void ClockWordController::SetWordRenderMode(ClockWrd *pWord, RenderMode mode)
{
	pWord->renderMode = mode;
	switch (pWord->renderMode)
	{
	case RenderMode::Off:
		break;

	case RenderMode::On:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentColour = renderColour;
		break;

	case RenderMode::Step:
		pWord->currentIndex = 0;
		pWord->currentColour = renderColour;
		break;

	case RenderMode::FadeOut:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentFadeStep = FADEOUT_STEPS;
		pWord->targetColour = RgbColor(0, 0, 0);
		break;

	case RenderMode::FadeIn:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentFadeStep = 0;
		pWord->targetColour = renderColour;
		break;

	case RenderMode::Flash:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentColour = renderColour;
		break;

	case RenderMode::SparkleIn:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentColour = renderColour;
		break;

	case RenderMode::SparkleOut:
		pWord->currentIndex = pWord->letterSize - 1;
		pWord->currentColour = renderColour;
		break;
	}
}

void ClockWordController::InitActiveWords()
{
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		activeWords[i].index = 0xFF;
		activeWords[i].renderMode = RenderMode::Off;
	}
}

void ClockWordController::DisplayClockWords(byte hour, byte min, byte sec, RenderMode renderMode, bool forceDisplay)
{
	int wordIndex = 0;
	int minuteIndex = GetWordForMinutes(min, sec);
	int hourIndex = GetWordForHours(hour, min, sec);

	// Use this to track if there's actually been a change in what we need to display
	byte oldDisplayIndexes[NUM_DISPLAY_WORDS];
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		oldDisplayIndexes[i] = activeWords[i].index;
	}

	// It's
	SetActiveWord(wordIndex++, 0, renderMode, renderColour);

	// About
	bool about = min % 5 != 0 && min % 5 != 1 && min % 5 != 4;
	if (about)
	{
		SetActiveWord(wordIndex++, 19, renderMode, renderColour);
	}

	// On the hour?
	if (minuteIndex == 13)
	{
		// Hour
		SetActiveWord(wordIndex++, hourIndex, renderMode, renderColour);
		// O'clock
		SetActiveWord(wordIndex++, minuteIndex, renderMode, renderColour);
		SetActiveWord(wordIndex++, 0xFF, RenderMode::Off, renderColour);
	}
	else
	{
		// Minutes
		// Suppress minute flashing if we're setting the hours
		SetActiveWord(wordIndex++, minuteIndex, renderMode, renderColour);

		// Past / To
		if ((min * 60) + sec < 1950)
			SetActiveWord(wordIndex++, 21, renderMode, renderColour);
		else
			SetActiveWord(wordIndex++, 22, renderMode, renderColour);

		// Hour
		SetActiveWord(wordIndex++, hourIndex, renderMode, renderColour);
		SetActiveWord(wordIndex++, 0xFF, RenderMode::Off, renderColour);
	}

	if (forceDisplay)
	{
		SetDisplayWords();
	}
	else
	{
		// Check for any changes and only render if we have to
		for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
		{
			if (oldDisplayIndexes[i] != activeWords[i].index)
			{
				SetDisplayWords();
				break;
			}
		}
	}
}

byte ClockWordController::GetSentenceToDisplay()
{
	// Figure out if there's a sentence we can display. Hacky approach because of dynamic memory allocation risks on Arduino
	byte sentenceMax = 0;
	byte sentenceChoices[NUM_SENTENCES];

	// Initialise the array
	for (byte i = 0; i < NUM_SENTENCES; i++)
		sentenceChoices[i] = 0xFF;

	// Add candidates to the list
	for (byte i = SENTENCE_START; i < SENTENCE_START + NUM_SENTENCES; i++)
	{
		if ((words[i].showOnDay == -1 && words[i].showOnMonth == -1) ||
				// Day and Month are zero-based but it's easier to remember in regular 1-based notation here
				(words[i].showOnDay == clockTime.Day() + 1 && words[i].showOnMonth == clockTime.Month() + 1))
		{
			sentenceChoices[sentenceMax++] = i;
		}
	}

	// Now pick a random one. If no sentences available, sentenceMax = 0 and return value will be 0xFF
	return sentenceChoices[random(0, sentenceMax)];
}

void ClockWordController::DisplaySentence(byte sentence, RenderMode renderMode)
{
	//Serial.print(clockTime.Hour());
	//Serial.print(":");
	//Serial.print(clockTime.Minute());
	//Serial.print(":");
	//Serial.print(clockTime.Second());
	//Serial.print(": DisplaySentence...");

	int wordIndex = 0;

	// Reset the "end of words" marker throughout
	for (byte i = 0; i < NUM_DISPLAY_WORDS; i++)
	{
		activeWords[i].index = 0xFF;
		activeWords[i].renderMode = RenderMode::Off;
		activeWords[i].colour = RgbColor(0, 0, 0);
	}

	// Pick a sentence
	//Serial.print(sentence);
	SetActiveWord(wordIndex++, sentence, renderMode, sentenceColour);

	SetDisplayWords();
	//Serial.println("...exiting.");
}

void ClockWordController::SetActiveWord(byte index, byte wordIndex, RenderMode mode, RgbColor colour)
{
	activeWords[index].index = wordIndex;
	activeWords[index].renderMode = mode;
	activeWords[index].colour = colour;
}

byte ClockWordController::GetWordForMinutes(byte mins, byte secs)
{
	short minuteIndex = ((mins * 60) + secs) / 150;
	return pgm_read_byte(minutesLookup + minuteIndex);
}

byte ClockWordController::GetWordForHours(byte hour, byte min, byte sec)
{
	if (hour == 0)
		hour = 12;
	else if (hour > 12)
		hour = hour - 12;

	if ((min * 60) + sec < 1950)
		return hour;
	else
		if (hour == 12)
			return 1;
		else
			return hour + 1;
}

void ClockWordController::SetDayNightMode()
{
	// Update the display brightness (if needed)
	if (clockTime.Hour() >= clockConfig.NightModeStartHour || clockTime.Hour() < clockConfig.NightModeEndHour)
		strip.SetBrightness(NIGHT_BRIGHTNESS);
	else
		strip.SetBrightness(DAY_BRIGHTNESS);
}