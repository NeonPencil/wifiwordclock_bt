#pragma once

enum RenderMode
{
	Off,
	On,
	Step,
	FadeIn,
	SparkleIn,
	FadeOut,
	SparkleOut,
	Flash
};