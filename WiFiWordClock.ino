#include "Constants.h"
#include "ClockWordController.h"

#include <FS.h>
#include <Wire.h>
#include <RtcDS3231.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <EEPROM.h>

extern DNSServer dnsServer;
extern ESP8266WebServer server;

ClockConfig clockConfig;
ClockWordController wordCtlr;

// Real time clock
RtcDS3231<TwoWire> clock(Wire);
RtcDateTime clockTime;
bool timeChange = false;
bool configSaveRequired = false;

byte colourTransitionCounter = 0;
byte sentenceTransitionCounter = 0;

bool sentenceActive = false;
bool ledState = false;

unsigned long previousSentenceMillis;
unsigned long previousLEDMillis;
unsigned long previousClockMillis;

void setup()
{
	Serial.begin(115200);
	delay(1000);
	Serial.println();
	Serial.print("setup: initialisation started...");

	previousLEDMillis = 0;
	previousSentenceMillis = 0;
	previousClockMillis = 0;
	sentenceActive = false;
	pinMode(12, OUTPUT);

	ShowSystemInfo();
	InitRTC();
	InitFileSystem();
	InitAccessPoint();
	InitWebServer();
	LoadSettings();

	wordCtlr.init(colours[clockConfig.ColourIndex], colours[clockConfig.SentenceDisplayColour]);

	Serial.println("complete.");
}

void LoadSettings()
{
	GetClockConfig();
}


void InitRTC()
{
	clock.Begin();
	Wire.begin(I2C_SDA, I2C_SCL);

	clockTime = clock.GetDateTime();
	timeChange = true;
}

void loop()
{
	// Update the clock
	unsigned long currentMillis = millis();
	if (currentMillis > previousClockMillis + (CLOCK_REFRESH_TIME * 1000))
	{
		previousClockMillis = currentMillis;
		clockTime = clock.GetDateTime();
		timeChange = true;
	}

	if (currentMillis > previousLEDMillis + 500)
	{
		previousLEDMillis = currentMillis;
		ledState = !ledState;
		digitalWrite(12, ledState);
	}

	if (configSaveRequired)
	{
		SaveClockConfig();
		configSaveRequired = false;
	}

	// Update the words
	wordCtlr.loop();

	// Update clock
	UpdateClock();

	// DNS
	dnsServer.processNextRequest();

	// Web
	server.handleClient();
}

void UpdateClock()
{
	unsigned long currentMillis = millis();

	if (SENTENCES_ENABLED)
	{
		// Is it time to display a sentence?
		long sentenceRate = GetSentenceDisplayRate();

		// Is there a sentence to display?
		byte sentence = wordCtlr.GetSentenceToDisplay();
		if (sentence != 0xFF)
		{
			if (sentenceRate > 0 && currentMillis > previousSentenceMillis + sentenceRate)
			{
				sentenceActive = true;
				previousSentenceMillis = currentMillis;
				wordCtlr.DisplaySentence(sentence, clockConfig.DisplayMode);
			}
			else
			{
				if (sentenceActive && (currentMillis > previousSentenceMillis + (clockConfig.SentenceDisplayTime * 1000)))
				{
					// Revert to normal time display
					timeChange = true;
					sentenceActive = false;
				}
			}
		}
	}

	if (!sentenceActive && timeChange == true)
	{
		// Update the colour (if needed)
		UpdateDisplayColour();

		// Build clock words to display
		wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), clockConfig.DisplayMode, false);
		timeChange = false;
	}
}

long GetSentenceDisplayRate()
{
	long sentenceRate = 0;

	switch (clockConfig.SentenceTransition)
	{
	case 0:
		break;

	case 1:
		// Every minute
		sentenceRate = 1 * 60000;
		break;

	case 2:
		// Every 5 minutes
		sentenceRate = 5 * 60000;
		break;

	case 3:
		// Every 15 minutes
		sentenceRate = 15 * 60000;
		break;

	case 4:
		// Every 30 minutes
		sentenceRate = 30 * 60000;
		break;

	case 5:
		// Every 60 minutes
		sentenceRate = 60 * 60000;
		break;

	default:
		break;
	}

	return sentenceRate;
}

void UpdateDisplayColour()
{
	// This gets called every 30 seconds
	switch (clockConfig.ColourTransition)
	{
	case 0:
		// No transition
		break;

	case 1:
		// Every 5 minutes
		if (colourTransitionCounter++ > (5 * 2))
		{
			UpdateColourTransition();
		}
		break;

	case 2:
		// Every 15 minutes
		if (colourTransitionCounter++ > (15 * 2))
		{
			UpdateColourTransition();
		}
		break;

	case 3:
		// Every 30 minutes
		if (colourTransitionCounter++ > (30 * 2))
		{
			UpdateColourTransition();
		}
		break;

	case 4:
		// Every 60 minutes
		if (colourTransitionCounter++ > (60 * 2))
		{
			UpdateColourTransition();
		}
		break;

	case 5:
		// Every 24 hours
		if (colourTransitionCounter++ > (24 * 60 * 2))
		{
			UpdateColourTransition();
		}
		break;
	}
}

void UpdateColourTransition()
{
	colourTransitionCounter = 0;
	clockConfig.ColourIndex++;
	if (clockConfig.ColourIndex >= (int)pgm_read_byte(&NUM_COLOURS))
	{
		clockConfig.ColourIndex = 0;
	}

	wordCtlr.SetDisplayColour(colours[clockConfig.ColourIndex]);
}

void GetClockConfig()
{
	// Initialise the default ClockConfig in case the config file doesn't exist
	clockConfig.ColourIndex = 0;
	clockConfig.DisplayMode = RenderMode::On;
	clockConfig.ColourTransition = 0;
	clockConfig.SentenceTransition = 0;
	clockConfig.SentenceDisplayTime = 10;
	clockConfig.SentenceDisplayColour = 4;
	clockConfig.NightModeStartHour = 18;
	clockConfig.NightModeEndHour = 6;

	File f = SPIFFS.open("/clock.conf", "r");
	if (!f)
	{
		// Save the default
		SaveClockConfig();
	}
	else
	{
		String tmp = f.readStringUntil('\n');
		clockConfig.ColourIndex = (byte)constrain(tmp.toInt(), 0, (int)(pgm_read_byte(&NUM_COLOURS) - 1));

		tmp = f.readStringUntil('\n');
		clockConfig.DisplayMode = (RenderMode)constrain(tmp.toInt(), 1, 4);

		tmp = f.readStringUntil('\n');
		clockConfig.ColourTransition = (byte)constrain(tmp.toInt(), 0, 5);

		tmp = f.readStringUntil('\n');
		clockConfig.SentenceTransition = (byte)constrain(tmp.toInt(), 0, 5);

		tmp = f.readStringUntil('\n');
		clockConfig.SentenceDisplayTime = (byte)constrain(tmp.toInt(), 5, 30);

		tmp = f.readStringUntil('\n');
		clockConfig.SentenceDisplayColour = (byte)constrain(tmp.toInt(), 0, (int)(pgm_read_byte(&NUM_COLOURS) - 1));

		tmp = f.readStringUntil('\n');
		clockConfig.NightModeStartHour = (byte)constrain(tmp.toInt(), 12, 23);

		tmp = f.readStringUntil('\n');
		clockConfig.NightModeEndHour = (byte)constrain(tmp.toInt(), 0, 11);

		f.close();

		Serial.print("colourIndex: ");
		Serial.println(clockConfig.ColourIndex);
		Serial.print("renderMode: ");
		Serial.println(clockConfig.DisplayMode);
		Serial.print("colourTransition: ");
		Serial.println(clockConfig.ColourTransition);
		Serial.print("sentenceTransition: ");
		Serial.println(clockConfig.SentenceTransition);
		Serial.print("sentenceDisplayTime: ");
		Serial.println(clockConfig.SentenceDisplayTime);
		Serial.print("sentenceDisplayColour: ");
		Serial.println(clockConfig.SentenceDisplayColour);
		Serial.print("nightModeStartHour: ");
		Serial.println(clockConfig.NightModeStartHour);
		Serial.print("nightModeEndHour: ");
		Serial.println(clockConfig.NightModeEndHour);
		Serial.print("current Hour: ");
		Serial.println(clockTime.Hour());
		Serial.print("current Minute: ");
		Serial.println(clockTime.Minute());
		Serial.print("current Day: ");
		Serial.println(clockTime.Day());
		Serial.print("current Month: ");
		Serial.println(clockTime.Month());

	}
}

void SaveClockConfig()
{
	File f = SPIFFS.open("/clock.conf", "w");
	if (f)
	{
		f.println(clockConfig.ColourIndex);
		f.println(clockConfig.DisplayMode);
		f.println(clockConfig.ColourTransition);
		f.println(clockConfig.SentenceTransition);
		f.println(clockConfig.SentenceDisplayTime);
		f.println(clockConfig.SentenceDisplayColour);
		f.println(clockConfig.NightModeStartHour);
		f.println(clockConfig.NightModeEndHour);
		f.close();
	}
}