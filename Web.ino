extern "C" {
#include "user_interface.h"
}

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <FS.h>

// AP mode password
const char WiFiAPPSK[] = "";

// DNS server
const byte DNS_PORT = 53;
DNSServer dnsServer;

// Web server
ESP8266WebServer server(80);

// Soft AP network parameters
IPAddress accessPointIP(192, 168, 4, 1);
IPAddress netMask(255, 255, 255, 0);

void ShowSystemInfo()
{
	Serial.println();
	Serial.print(F("Heap: ")); Serial.println(system_get_free_heap_size());
	Serial.print(F("Boot Vers: ")); Serial.println(system_get_boot_version());
	Serial.print(F("CPU: ")); Serial.println(system_get_cpu_freq());
	Serial.print(F("SDK: ")); Serial.println(system_get_sdk_version());
	Serial.print(F("Chip ID: ")); Serial.println(system_get_chip_id());
	Serial.print(F("Flash ID: ")); Serial.println(spi_flash_get_id());
	Serial.print(F("Flash Size: ")); Serial.println(ESP.getFlashChipRealSize());
	Serial.print(F("Vcc: ")); Serial.println(ESP.getVcc());
	Serial.println();
}

void InitFileSystem()
{
	SPIFFS.begin();

	Dir dir = SPIFFS.openDir("/");
	while (dir.next()) {
		String fileName = dir.fileName();
		size_t fileSize = dir.fileSize();
		Serial.printf("FS File: %s, size: %s\n", fileName.c_str(), String(fileSize).c_str());
	}

	Serial.printf("\n");
}

void InitAccessPoint()
{
	WiFi.mode(WIFI_AP);

	// Do a little work to get a unique-ish name. Append the last two bytes of the MAC (HEX'd) to "WordClock-":
	uint8_t mac[WL_MAC_ADDR_LENGTH];
	WiFi.softAPmacAddress(mac);
	String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) + String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	macID.toUpperCase();
	String AP_NameString = "WordClock-" + macID;

	char AP_NameChar[AP_NameString.length() + 1];
	memset(AP_NameChar, 0, AP_NameString.length() + 1);

	for (int i = 0; i < AP_NameString.length(); i++)
		AP_NameChar[i] = AP_NameString.charAt(i);

	WiFi.softAPConfig(accessPointIP, accessPointIP, netMask);
	WiFi.softAP(AP_NameChar);

	/* Setup the DNS server redirecting all the domains to the apIP */
	dnsServer.setTTL(300);
	dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);
	dnsServer.start(DNS_PORT, "*", accessPointIP);

	Serial.println("WifFi access point created.");
}

void InitWebServer()
{
	server.on("/setTime", HTTP_POST, []()
	{
		byte hour = server.arg("hh") == NULL ? 0 : (byte)server.arg("hh").toInt();
		byte minute = server.arg("mm") == NULL ? 0 : (byte)server.arg("mm").toInt();
		byte day = server.arg("d") == NULL ? 0 : (byte)server.arg("d").toInt();
		byte month = server.arg("m") == NULL ? 0 : (byte)server.arg("m").toInt();
		int year = server.arg("y") == NULL ? 0 : server.arg("y").toInt();
		SetTime(hour, minute, day, month, year);
	});

	server.on("/getTime", HTTP_GET, []()
	{
		GetTime();
	});

	server.on("/setColour", HTTP_POST, []()
	{
		byte colour = server.arg("c") == NULL ? 0 : (byte)server.arg("c").toInt();
		SetDisplayColour(colour);
	});

	server.on("/getColour", HTTP_GET, []()
	{
		GetColour();
	});

	server.on("/setTransition", HTTP_POST, []()
	{
		byte wordTransition = server.arg("w") == NULL ? 1 : (byte)server.arg("w").toInt();
		byte colourTransition = server.arg("c") == NULL ? 0 : (byte)server.arg("c").toInt();
		byte nightStart = server.arg("nms") == NULL ? 18 : (byte)server.arg("nms").toInt();
		byte nightEnd = server.arg("nme") == NULL ? 0 : (byte)server.arg("nme").toInt();
		SetTransition(wordTransition, colourTransition, nightStart, nightEnd, false);
	});

	server.on("/saveTransition", HTTP_POST, []()
	{
		byte wordTransition = server.arg("w") == NULL ? 1 : (byte)server.arg("w").toInt();
		byte colourTransition = server.arg("c") == NULL ? 0 : (byte)server.arg("c").toInt();
		byte nightStart = server.arg("nms") == NULL ? 18 : (byte)server.arg("nms").toInt();
		byte nightEnd = server.arg("nme") == NULL ? 0 : (byte)server.arg("nme").toInt();
		SetTransition(wordTransition, colourTransition, nightStart, nightEnd, true);
	});

	server.on("/getTransition", HTTP_GET, []()
	{
		GetTransition();
	});

	if (SENTENCES_ENABLED)
	{
		server.on("/setSentence", HTTP_POST, []()
		{
			byte sentenceTransition = server.arg("s") == NULL ? 0 : (byte)server.arg("s").toInt();
			byte sentenceTime = server.arg("t") == NULL ? 0 : (byte)server.arg("t").toInt();
			byte sentenceCol = server.arg("c") == NULL ? 0 : (byte)server.arg("c").toInt();
			SetSentence(sentenceTransition, sentenceTime, sentenceCol);
		});

		server.on("/getSentence", HTTP_GET, []()
		{
			GetSentence();
		});
	}

	//if (SENTENCES_ENABLED)
	//{
	//	server.serveStatic("/", SPIFFS, "/default.html");
	//	server.serveStatic("/default.html", SPIFFS, "/default.html");
	//	server.serveStatic("/sentence.html", SPIFFS, "/sentence.html");
	//}
	//else
	//{
	//	server.serveStatic("/", SPIFFS, "/default.html");
	//	server.serveStatic("/default.html", SPIFFS, "/default.html");
	//}

	//server.serveStatic("/time.html", SPIFFS, "/time.html");
	//server.serveStatic("/colour.html", SPIFFS, "/colour.html");
	//server.serveStatic("/transition.html", SPIFFS, "/transition.html");
	//server.serveStatic("/fonts", SPIFFS, "/fonts", "max-age=86400");
	//server.serveStatic("/js", SPIFFS, "/js", "max-age=86400");
	//server.serveStatic("/css", SPIFFS, "/css", "max-age=86400");
	//server.serveStatic("/images", SPIFFS, "/images", "max-age=86400");

	// If the client requests any URI send it if it exists otherwise, respond with a 404 (Not Found) error
	server.onNotFound([]() {
		if (!handleFileRead(server.uri()))
			server.send(404, "text/plain", "WordClock couldn't find the requested file...");
	});

	server.begin();

	Serial.println("Web Server started.");
}

//
//
//
bool handleFileRead(String path)
{
	if (path.endsWith("/"))
		path += "default.html";
	String contentType = getContentType(path);
	String pathWithGz = path + ".gz";
	if (SPIFFS.exists(pathWithGz))
		path = pathWithGz;

	if (SPIFFS.exists(path))
	{
		File file = SPIFFS.open(path, "r");
		size_t sent = server.streamFile(file, contentType);
		size_t contentLength = file.size();
		file.close();
		return true;
	}

	return false;
}

//
//
//
String getContentType(String filename) {
	if (filename.endsWith(".htm")) return "text/html";
	else if (filename.endsWith(".html")) return "text/html";
	else if (filename.endsWith(".css")) return "text/css";
	else if (filename.endsWith(".js")) return "application/javascript";
	else if (filename.endsWith(".png")) return "image/png";
	else if (filename.endsWith(".gif")) return "image/gif";
	else if (filename.endsWith(".jpg")) return "image/jpeg";
	else if (filename.endsWith(".ico")) return "image/x-icon";
	else if (filename.endsWith(".xml")) return "text/xml";
	else if (filename.endsWith(".pdf")) return "application/x-pdf";
	else if (filename.endsWith(".zip")) return "application/x-zip";
	else if (filename.endsWith(".gz")) return "application/x-gzip";
	else if (filename.endsWith(".svg")) return "image/svg+xml";
	else if (filename.endsWith(".eot")) return "application/vnd.ms-fontobject";
	else if (filename.endsWith(".ttf")) return "application/font-ttf";
	else if (filename.endsWith(".woff")) return "application/font-woff";
	else if (filename.endsWith(".woff2")) return "application/font-woff2";
	else if (filename.endsWith(".otf")) return "application /font-otf";
	return "text/plain";
}

void SetTime(byte hour, byte minute, byte day, byte month, int year)
{
	bool invalid = false;

	if (hour < 1 || hour > 23) {
		invalid = true;
	}

	if (minute < 0 || minute > 59) {
		invalid = true;
	}

	if (day < 1 || day > 31) {
		invalid = true;
	}

	if (month < 0 || month > 11) {
		invalid = true;
	}

	if (year < 1900 || year > 2100) {
		invalid = true;
	}

	hour = constrain(hour, 1, 23);
	minute = constrain(minute, 0, 59);
	day = constrain(day, 1, 31);
	month = constrain(month, 0, 11);
	year = constrain(year, 1900, 2100);

	RtcDateTime newTime(year, month, day, hour, minute, 0);
	clock.SetDateTime(newTime);
	clockTime = clock.GetDateTime();
	wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), clockConfig.DisplayMode, true);

	String result = invalid == false ? "success" : "fail";
	String json = "{";
	json += "\"status\": \"" + result + "\"";
	if (invalid)
		json += ",\"errMsg\": \"An invalid hour or minute value was sent from the browser.\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void GetTime()
{
	String json = "{";
	json += "\"status\": \"success\"";
	json += ",\"h\": \"" + String(clockTime.Hour()) + "\"";
	json += ",\"m\": \"" + String(clockTime.Minute()) + "\"";
	json += ",\"dd\": \"" + String(clockTime.Day()) + "\"";
	json += ",\"mm\": \"" + String(clockTime.Month()) + "\"";
	json += ",\"yy\": \"" + String(clockTime.Year()) + "\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void SetDisplayColour(byte colour)
{
	bool invalid = false;

	if (colour < 0 || colour > pgm_read_byte(&NUM_COLOURS) - 1) {
		invalid = true;
	}

	clockConfig.ColourIndex = constrain(colour, 0, pgm_read_byte(&NUM_COLOURS) - 1);
	wordCtlr.SetDisplayColour(colours[clockConfig.ColourIndex]);

	// Save the config to SPIFFS (for wear levelling). Note can't seem to access SPIFFs related functions
	// from within a method called by server.on
	configSaveRequired = true;

	String result = invalid == false ? "success" : "fail";
	String json = "{";
	json += "\"status\": \"" + result + "\"";
	if (invalid)
		json += ",\"errMsg\": \"An invalid colour value was sent from the browser.\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void GetColour()
{
	String json = "{";
	json += "\"status\": \"success\"";
	json += ",\"errMsg\": \"\"";
	json += ",\"c\": \"" + String(clockConfig.ColourIndex) + "\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void SetTransition(byte wordTransition, byte colTransition, byte nightStart, byte nightEnd, bool save)
{
	bool invalid = false;

	// Word transition
	if (wordTransition < 1 || wordTransition > 4) {
		invalid = true;
	}

	// Override for demo/display purposes
	clockConfig.OverrideDisplayMode = (RenderMode)constrain(wordTransition, 1, 4);
	wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), RenderMode::Off, true);
	wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), clockConfig.OverrideDisplayMode, true);

	// Only worry about validating if we're going to save as these options provide no user feedback
	// on the web page
	if (save)
	{
		clockConfig.DisplayMode = (RenderMode)constrain(wordTransition, 1, 4);

		// Colour transition
		if (colTransition < 0 || colTransition > 5) {
			invalid = true;
		}

		colTransition = constrain(colTransition, 0, 5);
		clockConfig.ColourTransition = colTransition;

		// Night mode start
		if (nightStart < 12 || nightStart > 23) {
			invalid = true;
		}

		nightStart = constrain(nightStart, 12, 23);
		clockConfig.NightModeStartHour = nightStart;

		// Night mode end
		if (nightEnd < 0 || nightEnd > 12) {
			invalid = true;
		}

		nightEnd = constrain(nightEnd, 0, 12);
		clockConfig.NightModeEndHour = nightEnd;

		// Save the config to SPIFFS for wear levelling. Note can't seem to access SPIFFs related
		// functions from within a method called by server.on, so we set a save flag here and write to
		// SPIFFS from the main loop
		configSaveRequired = true;
	}

	// Return result
	String result = invalid == false ? "success" : "fail";
	String json = "{";
	json += "\"status\": \"" + result + "\"";
	if (invalid)
		json += ",\"errMsg\": \"An invalid transition value was sent from the browser.\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

//
//
//
void GetTransition()
{
	String json = "{";
	json += "\"status\": \"success\"";
	json += ",\"errMsg\": \"\"";
	json += ",\"w\": \"" + String(clockConfig.DisplayMode) + "\"";
	json += ",\"c\": \"" + String(clockConfig.ColourTransition) + "\"";
	json += ",\"nms\": \"" + String(clockConfig.NightModeStartHour) + "\"";
	json += ",\"nme\": \"" + String(clockConfig.NightModeEndHour) + "\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void SetSentence(byte sentenceTrans, byte sentenceTime, byte sentenceCol)
{
	bool invalid = false;

	// Sentence transition
	if (sentenceTrans < 0 || sentenceTrans > 5) {
		invalid = true;
	}

	sentenceTrans = constrain(sentenceTrans, 0, 5);
	clockConfig.SentenceTransition = sentenceTrans;

	// Sentence display time
	if (sentenceTime < 10 || sentenceTime > 30) {
		invalid = true;
	}

	sentenceTime = constrain(sentenceTime, 10, 30);
	clockConfig.SentenceDisplayTime = sentenceTime;

	// Sentence colour
	if (sentenceCol < 0 || sentenceCol > pgm_read_byte(&NUM_COLOURS) - 1) {
		invalid = true;
	}

	sentenceCol = constrain(sentenceCol, 0, pgm_read_byte(&NUM_COLOURS) - 1);
	clockConfig.SentenceDisplayColour = sentenceCol;
	wordCtlr.SetSentenceColour(colours[clockConfig.SentenceDisplayColour]);

	// Save the config to SPIFFS (for wear levelling). Note can't seem to access SPIFFs related functions
	// from within a method called by server.on
	configSaveRequired = true;

	String result = invalid == false ? "success" : "fail";
	String json = "{";
	json += "\"status\": \"" + result + "\"";
	if (invalid)
	json += ",\"errMsg\": \"An invalid transition or display value was sent from the browser.\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}

void GetSentence()
{
	String json = "{";
	json += "\"status\": \"success\"";
	json += ",\"errMsg\": \"\"";
	json += ",\"s\": \"" + String(clockConfig.SentenceTransition) + "\"";
	json += ",\"t\": \"" + String(clockConfig.SentenceDisplayTime) + "\"";
	json += ",\"c\": \"" + String(clockConfig.SentenceDisplayColour) + "\"";
	json += "}";
	server.send(200, "text/json", json);
	json = String();
}