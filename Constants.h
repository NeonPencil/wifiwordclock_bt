// Constants.h

#ifndef _CONSTANTS_h
#define _CONSTANTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "ClockWordController.h"

#define CLOCK_REFRESH_TIME 30
#define I2C_SDA 4
#define I2C_SCL 5

#endif
