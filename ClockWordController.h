// ClockWordController.h

#ifndef _CLOCKWORDCONTROLLER_h
#define _CLOCKWORDCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <NeoPixelBrightnessBus.h>
//#include "Constants.h"
#include "ClockConfig.h"
#include "RenderMode.h"

#define NUM_CLOCK_WORDS 58
#define NUM_LEDS 15 * 11
#define DISPLAY_DATA_PIN 15
#define NUM_DISPLAY_WORDS 6 + 1		// 3 words plus one for the terminator (0xFF)

#define DAY_BRIGHTNESS 24
#define NIGHT_BRIGHTNESS 6
#define SENTENCES_ENABLED 1

// Refresh rates for the display controller
const word STEP_RATE PROGMEM = 100;
const word SPARKLE_RATE PROGMEM = 50;
const word FLASH_RATE PROGMEM = 500;
const word FADE_RATE PROGMEM = 25;
const word REFRESH_RATE PROGMEM = 50;

const byte FADEIN_STEPS PROGMEM = 25;
const byte FADEOUT_STEPS PROGMEM = 50;
const byte SENTENCE_START PROGMEM = 23;
const byte NUM_SENTENCES PROGMEM = 35;

struct DisplayWord
{
	byte index;
	RenderMode renderMode;
	RgbColor colour;
};

struct ClockWrd
{
	int showOnMonth;
	int showOnDay;
	const byte * letters;
	size_t letterSize;
	byte currentIndex;
	RenderMode renderMode;
	byte currentFadeStep;
	RgbColor currentColour;
	RgbColor targetColour;
};

class ClockWordController
{
protected:

public:
	void init(RgbColor displayCol, RgbColor sentenceCol);
	void loop();
	void UpdateTimers();
	void UpdateClockWords();
	void Render();
	void RenderFade();
	void RenderSparkleOut();
	void RenderNormal();

	void SetDisplayWords();
	void SetDisplayColour(RgbColor colour);
	void SetSentenceColour(RgbColor colour);
	void DisplayClockWords(byte hour, byte min, byte sec, RenderMode renderMode, bool forceDisplay);
	byte GetSentenceToDisplay();
	void DisplaySentence(byte sentence, RenderMode renderMode);
	void InitActiveWords();

	void SetDayNightMode();
	void SetWordRenderMode(ClockWrd *pWord, RenderMode mode);
	void SetWordRenderColour(ClockWrd *pWord, RgbColor colour);
	void UpdateWord(ClockWrd *pWord, bool updateStep, bool updateFlash, bool updateFade, bool updateSparkle);
	void RenderClockWord(ClockWrd *pWord);

	void SetActiveWord(byte index, byte wordIndex, RenderMode mode, RgbColor colour);
	byte GetWordForMinutes(byte mins, byte secs);
	byte GetWordForHours(byte hour, byte min, byte sec);

private:
	RgbColor leds[NUM_LEDS];
	RgbColor renderColour;
	RgbColor sentenceColour;
	RgbColor currentColour;
	RgbColor targetColour;
	
	bool updateDisplay;
	bool updateStep;
	bool updateSparkle;
	bool updateFlash;
	bool updateFade;
	bool fadeoutActive;
	bool sparkleOutActive;
	bool flashState;

	DisplayWord activeWords[NUM_DISPLAY_WORDS];
	DisplayWord currentDisplayWords[NUM_DISPLAY_WORDS];
	DisplayWord newDisplayWords[NUM_DISPLAY_WORDS];

	// General purpose timing
	unsigned long currentMillis;
	unsigned long previousMillis;
	unsigned long previousStepMillis;
	unsigned long previousSparkleMillis;
	unsigned long previousFlashMillis;
	unsigned long previousFadeMillis;

	DisplayWord currentDisplayLetters[NUM_LEDS];
	DisplayWord newDisplayLetters[NUM_LEDS];
	byte currentLetterCount;
	byte currentLetterIndex;
	byte newLetterCount;

	// Word storage
	ClockWrd words[NUM_CLOCK_WORDS];
};

const RgbColor colours[] = {
	RgbColor(255, 255, 255),
	RgbColor(255, 0, 0),
	RgbColor(128, 0, 0),
	RgbColor(255, 128, 0),
	RgbColor(255, 80, 0),
	RgbColor(127, 255, 0),
	RgbColor(0, 255, 0),
	RgbColor(0, 255, 127),
	RgbColor(0, 255, 255),
	RgbColor(0, 127, 255),
	RgbColor(0, 0, 255),
	RgbColor(127, 0, 255),
	RgbColor(255, 0, 255),
	RgbColor(255, 0, 127)
};

const byte NUM_COLOURS PROGMEM = sizeof(colours) / sizeof(RgbColor);

const byte minutesLookup[] PROGMEM = { 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 20, 20, 18, 18, 17, 17, 16, 16, 15, 15, 14, 14, 13 };

// Its
const byte letters0[] PROGMEM = { 2, 3, 4, 0xFF };

// One
const byte letters1[] PROGMEM = { 147, 146, 145, 0xFF };

// Two
const byte letters2[] PROGMEM = { 107, 106, 105, 0xFF };

// Three
const byte letters3[] PROGMEM = { 70, 71, 72, 73, 74, 0xFF };

// Four
const byte letters4[] PROGMEM = { 89, 88, 87, 86, 0xFF };

// Five
const byte letters5[] PROGMEM = { 150, 151, 152, 153, 0xFF };

// Six
const byte letters6[] PROGMEM = { 126, 127, 128, 0xFF };

// Seven
const byte letters7[] PROGMEM = { 119, 118, 117, 116, 115, 0xFF };

// Eight
const byte letters8[] PROGMEM = { 145, 144, 143, 142, 141, 0xFF };

// Nine
const byte letters9[] PROGMEM = { 85, 84, 83, 82, 0xFF };

// Ten
const byte letters10[] PROGMEM = { 141, 140, 139, 0xFF };

// Eleven
const byte letters11[] PROGMEM = { 129, 130, 131, 132, 133, 134, 0xFF };

// Twelve
const byte letters12[] PROGMEM = { 99, 100, 101, 102, 103, 104, 0xFF };

// O'Clock
const byte letters13[] PROGMEM = { 159, 160, 161, 162, 163, 164, 0xFF };

// Five (mins)
const byte letters14[] PROGMEM = { 36, 37, 38, 39, 0xFF };

// Ten (mins)
const byte letters15[] PROGMEM = { 30, 31, 32, 0xFF };

// A Quarter
const byte letters16[] PROGMEM = { 34, 56, 55, 54, 53, 52, 51, 50, 0xFF };

// Twenty
const byte letters17[] PROGMEM = { 20, 19, 18, 17, 16, 15, 0xFF };

// Twentyfive 
const byte letters18[] PROGMEM = { 20, 19, 18, 17, 16, 15, 36, 37, 38, 39, 0xFF };

// About
const byte letters19[] PROGMEM = { 6, 7, 8, 9, 10, 0xFF };

// Half
const byte letters20[] PROGMEM = { 33, 34, 35, 36, 0xFF };

// Past
const byte letters21[] PROGMEM = { 63, 64, 65, 66, 0xFF };

// To
const byte letters22[] PROGMEM = { 66, 67, 0xFF };

// Start sentences here

// We love Tiles
const byte letters23[] PROGMEM = { 19, 18, 40, 41, 42, 43, 49, 48, 47, 46, 45, 0xFF };

// We love Tile Ideas
const byte letters24[] PROGMEM = { 19, 18, 40, 41, 42, 43, 49, 48, 47, 46, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Tiles
const byte letters25[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 49, 48, 47, 46, 45, 0xFF };

// Think Beaumonts for Tile Ideas
const byte letters26[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 49, 48, 47, 46, 154, 155, 156, 157, 158, 0xFF };

// We love Outdoor
const byte letters27[] PROGMEM = { 19, 19, 18, 40, 41, 42, 43, 81, 80, 79, 78, 77, 76, 75, 0xFF };

// We love our Outdoor Ideas
const byte letters28[] PROGMEM = { 19, 18, 40, 41, 42, 43, 81, 80, 79, 78, 77, 76, 75, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Outdoor
const byte letters29[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 81, 80, 79, 78, 77, 76, 75, 0xFF };

// Think Beaumonts for Outdoor ideas
const byte letters30[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 81, 80, 79, 78, 77, 76, 75, 154, 155, 156, 157, 158, 0xFF };

// We love Bathrooms
const byte letters31[] PROGMEM = { 19, 18, 40, 41, 42, 43, 90, 91, 92, 93, 94, 95, 96, 97, 98, 0xFF };

// We love our Bathrooms
const byte letters32[] PROGMEM = { 19, 18, 40, 41, 42, 43, 90, 91, 92, 93, 94, 95, 96, 97, 98, 88, 87, 86, 0xFF };

// We love our Bathroom Ideas
const byte letters33[] PROGMEM = { 19, 19, 18, 40, 41, 42, 43, 88, 87, 86, 90, 91, 92, 93, 94, 95, 96, 97, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Bathrooms
const byte letters34[] PROGMEM = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 45, 46, 47, 90, 91, 92, 93, 94, 95, 96, 97, 98 , 0xFF};

// Think Beaumonts for Bathroom Ideas
const byte letters35[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 90, 91, 92, 93, 94, 95, 96, 97, 154, 155, 156, 157, 158, 0xFF };

// We love Bathroomware
const byte letters36[] PROGMEM = { 19, 18, 40, 41, 42, 43, 90, 91, 92, 93, 94, 95, 96, 97, 111, 110, 109, 108, 0xFF };

// We love our Bathroomware
const byte letters37[] PROGMEM = { 19, 18, 40, 41, 42, 43, 90, 91, 92, 93, 94, 95, 96, 97, 111, 110, 109, 108, 88, 87, 86, 0xFF };

// We love our Bathroomware Ideas
const byte letters38[] PROGMEM = { 19, 18, 40, 41, 42, 43, 90, 91, 92, 93, 94, 95, 96, 97, 111, 110, 109, 108, 88, 87, 86, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Bathroomware
const byte letters39[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 90, 91, 92, 93, 94, 95, 96, 97, 111, 110, 109, 108, 0xFF };

// Think Beaumonts for Bathroomware Ideas
const byte letters40[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 90, 91, 92, 93, 94, 95, 96, 97, 111, 110, 109, 108, 154, 155, 156, 157, 158, 0xFF };

// We love Tapware
const byte letters41[] PROGMEM = { 19, 18, 40, 41, 42, 43, 114, 113, 112, 111, 110, 109, 108, 0xFF };

// We love our Tapware
const byte letters42[] PROGMEM = { 19, 18, 40, 41, 42, 43, 114, 113, 112, 111, 110, 109, 108, 88, 87, 86, 0xFF };

// We love our Tapware Ideas
const byte letters43[] PROGMEM = { 19, 18, 40, 41, 42, 43, 114, 113, 112, 111, 110, 109, 108, 88, 87, 86, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Tapware
const byte letters44[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 114, 113, 112, 111, 110, 109, 108, 0xFF };

// Think Beaumonts for Tapware Ideas
const byte letters45[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 114, 113, 112, 111, 110, 109, 108, 154, 155, 156, 157, 158, 0xFF };

// We love Stone
const byte letters46[] PROGMEM = { 19, 18, 40, 41, 42, 43, 149, 148, 147, 146, 145, 0xFF };

// We love our Stone
const byte letters47[] PROGMEM = { 19, 18, 40, 41, 42, 43, 149, 148, 147, 146, 145, 88, 87, 86, 0xFF };

// We love our Stone Ideas
const byte letters48[] PROGMEM = { 19, 18, 40, 41, 42, 43, 149, 148, 147, 146, 145, 88, 87, 86, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Stone
const byte letters49[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 149, 148, 147, 146, 145, 0xFF };

// Think Beaumonts for Stone Ideas
const byte letters50[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 149, 148, 147, 146, 145, 154, 155, 156, 157, 158, 0xFF };

// We love our Room Ideas
const byte letters51[] PROGMEM = { 19, 18, 40, 41, 42, 43, 88, 87, 86, 94, 95, 96, 97, 154, 155, 156, 157, 158, 0xFF };

// Think Beaumonts for Room Ideas
const byte letters52[] PROGMEM = { 10, 11, 12, 13, 14, 29, 28, 27, 26, 25, 24, 23, 22, 21, 59, 58, 57, 94, 95, 96, 97, 154, 155, 156, 157, 158, 0xFF };

// Beaumont Tiles
const byte letters53[] PROGMEM = { 29, 28, 27, 26, 25, 24, 23, 22, 49, 48, 47, 46, 45, 0xFF };

// Tiles are Hot
const byte letters54[] PROGMEM = { 49, 48, 47, 46, 45, 60, 61, 62, 68, 69, 70, 0xFF };

// Tiles are Cool
const byte letters55[] PROGMEM = { 49, 48, 47, 46, 45, 60, 61, 62, 138, 137, 136, 135, 0xFF };

// Love our Coffee
const byte letters56[] PROGMEM = { 40, 41, 42, 43, 88, 87, 86, 120, 121, 122, 123, 124, 125, 0xFF };

// Lee loves Coffee
const byte letters57[] PROGMEM = { 1, 28, 31, 40, 41, 42, 43, 44, 120, 121, 122, 123, 124, 125, 0xFF };
#endif