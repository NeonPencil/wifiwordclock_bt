

//
//
//
void ToShowTime_Update()
{
	if (btnSet.isReleased())
	{
		clockTime = clock.GetDateTime();
		wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), renderMode, true);
		wordClockStateMachine.transitionTo(ShowTime);
	}
}

//
//
//
void State_Exit()
{
	// Reset active words here so everything is off. ShowTime_Update() will rebuild the
	// active words according to the current time
	wordCtlr.InitActiveWords();
}

//
//
//
void SetColour_Enter()
{
	clockTime = clock.GetDateTime();
	wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), RenderMode::Flash, true);
}

//
// Colour setting mode
//
void SetColour_Update()
{
	if (btnSet.wasPressed())
	{
		EEPROM.write(0, colourIndex);
		EEPROM.commit();
		wordClockStateMachine.transitionTo(ToShowTime);
	}

	if (btnUp.wasPressed())
	{
		if (colourIndex == pgm_read_byte(&NUM_COLOURS) - 1)
			colourIndex = 0;
		else
			colourIndex++;

		wordCtlr.SetDisplayColour(colours[colourIndex]);
	}

	if (btnDown.wasPressed())
	{
		if (colourIndex == 0)
			colourIndex = pgm_read_byte(&NUM_COLOURS) - 1;
		else
			colourIndex--;

		wordCtlr.SetDisplayColour(colours[colourIndex]);
	}
}

//
// Initialisation for the Set Minute state
//
void SetMinute_Enter()
{
	// Reset current words to remove "about"
	clockTime = clock.GetDateTime();
	timeSetHour = clockTime.Hour();
	timeSetMinute = (clockTime.Minute() / 5) * 5;

	wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
}

//
// Update loop when in Set Minute state
//
void SetMinute_Update()
{
	if (btnSet.wasPressed())
	{
		// Set minutes
		//clock.SetDateTime(1967, 3, 28, clockTime.Hour(), timeSetMinute, 0);
		RtcDateTime newTime(clockTime.Year(), clockTime.Month(), clockTime.Day(), clockTime.Hour(), timeSetMinute, 0);

		// Transition
		wordClockStateMachine.transitionTo(SetHour);
	}

	if (btnUp.wasPressed())
	{
		if (timeSetMinute == 55)
			timeSetMinute = 0;
		else
			timeSetMinute += 5;

		wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
	}

	if (btnDown.wasPressed())
	{
		if (timeSetMinute == 0)
			timeSetMinute = 55;
		else
			timeSetMinute -= 5;

		wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
	}
}

//
// Initialisation for the Set Hour state
//
void SetHour_Enter()
{
	// Reset current words to remove "about"
	clockTime = clock.GetDateTime();
	timeSetHour = clockTime.Hour();
	timeSetMinute = clockTime.Minute();

	wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
}

//
// Update loop when in Set Hour state
//
void SetHour_Update()
{
	if (btnSet.wasPressed())
	{
		// Set hours
		RtcDateTime newTime(clockTime.Year(), clockTime.Month(), clockTime.Day(), timeSetHour, clockTime.Minute(), 0);
		clock.SetDateTime(newTime);
		wordClockStateMachine.transitionTo(ToShowTime);
	}

	if (btnUp.wasPressed())
	{
		if (timeSetHour == 23)
			timeSetHour = 0;
		else
			timeSetHour++;

		wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
	}

	if (btnDown.wasPressed())
	{
		if (timeSetHour == 0)
			timeSetHour = 23;
		else
			timeSetHour--;

		wordCtlr.DisplayClockWords(timeSetHour, timeSetMinute, 0, RenderMode::Flash, true);
	}
}

//
//
//
void ShowTime_Enter()
{
	// Reset active words here so everything is off. ShowTime_Update() will rebuild the
	// active words according to the current time
	wordCtlr.InitActiveWords();
}

//
//
//
void ShowTime_Update()
{
	if (btnSet.wasReleased())
	{
		wordClockStateMachine.transitionTo(SetColour);
	}
	else if (btnSet.pressedFor(1000))
	{
		// Transition
		wordClockStateMachine.transitionTo(SetMinute);
	}

	unsigned long currentMillis = millis();
	// Is it time to display a sentence?
	long sentenceRate = 0;
	switch (sentenceTransition)
	{
	case 0:
		break;

	case 1:
		// Every 5 minutes
		sentenceRate = 5 * 60000;
		break;

	case 2:
		// Every 15 minutes
		sentenceRate = 15 * 60000;
		break;

	case 3:
		// Every 30 minutes
		sentenceRate = 30 * 60000;
		break;

	case 4:
		// Every 60 minutes
		sentenceRate = 60 * 60000;
		break;
	}

	if (sentenceRate > 0 && currentMillis - previousSentenceMillis >= sentenceRate)
	{
		sentenceActive = true;
		previousSentenceMillis = currentMillis;
		wordCtlr.DisplaySentence(renderMode);
	}
	else
	{
		if (sentenceActive && (previousSentenceMillis + SENTENCE_DISPLAY_TIME < currentMillis))
		{
			// Revert to normal time display
			timeChange = true;
			sentenceActive = false;
		}
	}

	if (!sentenceActive && timeChange == true)
	{
		// Update the colour (if needed)
		UpdateDisplayColour();

		// Build clock words to display
		wordCtlr.DisplayClockWords(clockTime.Hour(), clockTime.Minute(), clockTime.Second(), renderMode, false);
		timeChange = false;
	}
}

void UpdateDisplayColour()
{
	// This gets called every 30 seconds
	switch (colourTransition)
	{
		case 0:
			// No transition
			break;

		case 1:
			// Every 5 minutes
			if (colourTransitionCounter++ > (5 * 2))
			{
				UpdateColourTransition();
			}
			break;

		case 2:
			// Every 15 minutes
			if (colourTransitionCounter++ > (15 * 2))
			{
				UpdateColourTransition();
			}
			break;

		case 3:
			// Every 30 minutes
			if (colourTransitionCounter++ > (30 * 2))
			{
				UpdateColourTransition();
			}
			break;

		case 4:
			// Every 60 minutes
			if (colourTransitionCounter++ > (60 * 2))
			{
				UpdateColourTransition();
			}
			break;

		case 5:
			// Every 24 hours
			if (colourTransitionCounter++ > (24 * 60 * 2))
			{
				UpdateColourTransition();
			}
			break;
	}
}

void UpdateColourTransition()
{
	colourTransitionCounter = 0;
	colourIndex++;
	if (colourIndex >= (int)pgm_read_byte(&NUM_COLOURS))
	{
		colourIndex = 0;
	}

	wordCtlr.SetDisplayColour(colours[colourIndex]);
}