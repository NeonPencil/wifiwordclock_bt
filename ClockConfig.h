#pragma once

#include "RenderMode.h"

typedef struct ClockConfig
{
	byte ColourIndex;
	RenderMode DisplayMode;
	RenderMode OverrideDisplayMode;
	byte ColourTransition;
	byte SentenceTransition;
	byte SentenceDisplayTime;
	byte SentenceDisplayColour;
	byte NightModeStartHour;
	byte NightModeEndHour;
};